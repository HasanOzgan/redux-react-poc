import { BRANCH_SELECTED } from 'constants/branches';

export default {
  selectBranch: (selected) => ({ type : BRANCH_SELECTED, payload: selected })
};
