import { SELECT_CUSTOMER, UPDATE_CUSTOMER, OPEN_CUSTOMER_FORM, OPEN_CUSTOMER_CARD } from 'constants/customer';

export default {
  openCustomerForm: () => ({ type: OPEN_CUSTOMER_FORM }),
  openCustomerCard: () => ({ type: OPEN_CUSTOMER_CARD }),
  selectCustomer: (customer) => ({ type: SELECT_CUSTOMER, payload: customer }),
  updateCustomer: (customer) => ({ type: UPDATE_CUSTOMER, payload: customer })
};
