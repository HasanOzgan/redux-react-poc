import React                  from 'react';
import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';
import customerActions        from 'actions/customer';

// We define mapStateToProps and mapDispatchToProps where we'd normally use
// the @connect decorator so the data requirements are clear upfront, but then
// export the decorated component after the main class definition so
// the component can be tested w/ and w/o being connected.
// See: http://rackt.github.io/redux/docs/recipes/WritingTests.html
const mapStateToProps = (state) => ({
  routerState : state.router
});
const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(customerActions, dispatch)
});

class CustomerItem extends React.Component {
  static propTypes = {
    actions  : React.PropTypes.object,
    customer  : React.PropTypes.object
  }

  render () {
    let customer = this.props.customer;

    return (
        <a href="#" onClick={this.props.actions.selectCustomer.bind(true, customer)} className="list-group-item">
            <span className="left">
              <img className="avatar" src={customer.avatar} />
              <span className="customer-info">
                <span>{customer.firstname} {customer.lastname}</span>
              </span>
            </span>
        </a>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerItem);

