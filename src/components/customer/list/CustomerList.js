import React                  from 'react';
import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';
import customerActions         from 'actions/customer';
import CustomerItem           from './CustomerItem';
import CustomerService from '../../../services/CustomerService';

// We define mapStateToProps and mapDispatchToProps where we'd normally use
// the @connect decorator so the data requirements are clear upfront, but then
// export the decorated component after the main class definition so
// the component can be tested w/ and w/o being connected.
// See: http://rackt.github.io/redux/docs/recipes/WritingTests.html
const mapStateToProps = (state) => ({
  customer : state.customer,
  routerState : state.router
});
const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(customerActions, dispatch)
});

class CustomerList extends React.Component {
  static propTypes = {
    actions  : React.PropTypes.object,
    customer  : React.PropTypes.object
  }

  render () {
    var customers = [];

    for (var key in this.props.customer.items) {
      customers.push(<CustomerItem key={key} customer={this.props.customer.items[key]}/>);
    }

    return (
      <div className="panel panel-default">
        <div className="panel-heading"><strong>Customer List</strong></div>

        <div className="list-group">
          {customers}
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerList);