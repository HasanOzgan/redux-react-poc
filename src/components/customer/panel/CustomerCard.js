import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import customerActions from 'actions/customer';
import BranchService from 'services/BranchService';

const mapStateToProps = (state) => ({
    routerState : state.router
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(customerActions, dispatch)
});

class CustomerCard extends Component {

    static propTypes = {
        actions  : React.PropTypes.object,
        customer : React.PropTypes.object
    }

    render() {
      if (!this.props.visible) return null;
      else {      
        return (
          <form className="form-horizontal" role="form">
            <div className="form-group">
              <label htmlFor="firstname" className="col-sm-2 control-label">First Name</label>
              <div className="col-sm-10">
                <span>{this.props.customer.firstname}</span>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="lastname" className="col-sm-2 control-label">Last Name</label>
              <div className="col-sm-10">
                <span>{this.props.customer.lastname}</span>
              </div>
            </div>
            <div className="form-group">
                <label className="col-sm-2 control-label">Branch</label>
                <div className="col-sm-10">
                  <span>{BranchService.getTitleById(this.props.customer.branchId)}</span>
                </div>
            </div>
            <div className="form-group">
              <label htmlFor="lastname" className="col-sm-2 control-label">IBAN</label>
              <div className="col-sm-10">
                <span>{this.props.customer.iban}</span>
              </div>
            </div>

            <div className="form-group">
              <div className="col-sm-offset-2 col-sm-10">
                <button type="button" onClick={this.props.switchToForm} className="btn btn-primary">Edit</button>
              </div>
            </div>
          </form>
        );
      }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerCard);
