import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import customerActions from 'actions/customer';


const mapStateToProps = (state) => ({
  branch : state.branch,
  routerState : state.router
});

const mapDispatchToProps = (dispatch) => ({
    actions : bindActionCreators(customerActions, dispatch)
});

class CustomerForm extends Component {

  render() {
    if (!this.props.visible) return null;
    else {
      return (
        <form className="form-horizontal" role="form">
          <div className="form-group">
            <label htmlFor="firstname" className="col-sm-2 control-label">First Name</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" id="firstname" ref="firstname"  placeholder="Enter First Name" defaultValue={this.props.customer.firstname} />
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="lastname" className="col-sm-2 control-label">Last Name</label>
            <div className="col-sm-10">
              <input type="text" ref="lastname" className="form-control" id="lastname" placeholder="Enter Last Name"  defaultValue={this.props.customer.lastname} />
            </div>
          </div>
          <div className="form-group">
              <label className="col-sm-2 control-label">Branch</label>
              <div className="col-sm-10">
                <select ref="branch" className="branch form-control" defaultValue={this.props.customer.branchId}>
                  {this.props.branch.items.map(function(item, k) {
                      return <option key={k} value={item._id}>{item.title}</option>;
                  })}         
                </select>
              </div>
          </div>
          <div className="form-group">
            <label htmlFor="iban" className="col-sm-2 control-label">IBAN</label>
            <div className="col-sm-10">
              <input type="text" className="form-control" id="iban" ref="iban" placeholder="IBAN" defaultValue={this.props.customer.iban} />
            </div>
          </div>

          <div className="form-group">
            <div className="col-sm-offset-2 col-sm-10">
              <button type="button" onClick={this.onClick.bind(this)} className="btn btn-primary">Save</button>
            </div>
          </div>
        </form>
      );
    }
  }

  onClick(e) {
    e.preventDefault();

    let firstname = ReactDOM.findDOMNode(this.refs.firstname).value;
    let lastname = ReactDOM.findDOMNode(this.refs.lastname).value;
    let branchId = parseInt(ReactDOM.findDOMNode(this.refs.branch).value);
    let iban = ReactDOM.findDOMNode(this.refs.iban).value;    
    let customer = this.props.customer;

    customer = {...customer, iban:iban, firstname:firstname, branchId:branchId, lastname:lastname};
    console.log(customer);

    this.props.actions.updateCustomer(customer);
    this.props.switchToCard();
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerForm);
