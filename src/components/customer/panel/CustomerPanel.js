import React                  from 'react';
import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';
import customerActions         from 'actions/customer';
import CustomerService        from '../../../services/CustomerService';
import CustomerForm           from './CustomerForm';
import CustomerCard           from './CustomerCard';

// We define mapStateToProps and mapDispatchToProps where we'd normally use
// the @connect decorator so the data requirements are clear upfront, but then
// export the decorated component after the main class definition so
// the component can be tested w/ and w/o being connected.
// See: http://rackt.github.io/redux/docs/recipes/WritingTests.html
const mapStateToProps = (state) => ({
  customer : state.customer,
  editing : state.customer.editing,
  routerState : state.router
});
const mapDispatchToProps = (dispatch) => ({
  actions : bindActionCreators(customerActions, dispatch)
});

class CustomerPanel extends React.Component {
	static propTypes = {
		actions : React.PropTypes.object,
		editing : React.PropTypes.bool,
		customer : React.PropTypes.object
	}

  	render () {
		if (this.props.customer.selected._id) {
		    return (
				<div className="panel panel-default">
				  <div className="panel-heading"><strong>Customer Panel</strong></div>
				  <div className="panel-body">
					<CustomerForm visible={this.props.editing} customer={this.props.customer.selected} switchToCard={this.props.actions.openCustomerCard} />
					<CustomerCard visible={this.props.editing == false} customer={this.props.customer.selected} switchToForm={this.props.actions.openCustomerForm} />
				  </div>
				</div>
			);
		}
		else {
		    return (
				<div className="panel panel-default">
				  <div className="panel-heading"><strong>Customer Panel</strong></div>
				  <div className="panel-body">
					<span>Select a customer</span>
				  </div>
				</div>
			);
		}
  	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerPanel);