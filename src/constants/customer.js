import { createConstants } from '../utils';

export default createConstants(
  'SELECT_CUSTOMER',
  'UPDATE_CUSTOMER',
  'OPEN_CUSTOMER_FORM',
  'OPEN_CUSTOMER_CARD',
  'CUSTOMER_SELECTED',
  'CUSTOMER_UPDATED'
);
