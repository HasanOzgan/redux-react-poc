import React from 'react';
import Header from './Header';
import Footer from './Footer';
import '../assets/styles/app.scss';

export default class CoreLayout extends React.Component {
  static propTypes = {
    children : React.PropTypes.element
  }

  render () {
    return (
      <div className='layout-page'>
        <Header/>
        <main className='layout-main'>
          <div className='container'>
            {this.props.children}
          </div>
        </main>
        <Footer/>
      </div>
    );
  }
}
