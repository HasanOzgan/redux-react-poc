import React, {Component} from 'react';

export default class Footer extends Component {
  render () {
    return (
      <footer className='layout-footer'>
        <div className='container'>
          <div className='text-center'>&copy; 2015 Ziraat Bankası</div>
        </div>
      </footer>
    );
  }
}
