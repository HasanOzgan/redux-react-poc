import React, {Component} from 'react';
import {Navbar, NavBrand} from 'react-bootstrap';

export default class Header extends Component {
  render () {
    return (
      <Navbar>
        <NavBrand>
          <div>
            <a href='#'><img className='img-responsive' src='/assets/images/ziraat.png'/></a>
            <span className='poc-label'>PoC</span>
          </div>
        </NavBrand>
      </Navbar>
    );
  }
}
