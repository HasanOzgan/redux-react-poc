import { createReducer }     from '../utils';
import { BRANCH_SELECTED } from 'constants/branches';
import BranchService from 'services/BranchService';

const initialState = { items: BranchService.find(), selected: false};

export default createReducer(initialState, {
  [BRANCH_SELECTED] : (state, action) => {
    return {...state, selected: action};
  }
});
