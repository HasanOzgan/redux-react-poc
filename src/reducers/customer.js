import { createReducer }     from '../utils';
import CustomerService from '../services/CustomerService';
import { SELECT_CUSTOMER, UPDATE_CUSTOMER, OPEN_CUSTOMER_FORM, OPEN_CUSTOMER_CARD } from 'constants/customer';

export default createReducer({id:0}, {
  [SELECT_CUSTOMER] : (state) => { 
    console.log("customXXX");
    return {...state, id:1};
  }
});

const initialState = { items: CustomerService.find(), selected: false, editing: false };
export default createReducer(initialState, {
  [SELECT_CUSTOMER] : (state, selected) => { return {...state, selected: selected, editing: false}; },
  [OPEN_CUSTOMER_FORM] : (state) => { return {...state, editing: true}; },
  [OPEN_CUSTOMER_CARD] : (state) => { return {...state, editing: false}; },
  [UPDATE_CUSTOMER] : (state, selected) => { state.items[selected._id] = selected; return {items:state.items, selected: selected, editing: false}; }
});

