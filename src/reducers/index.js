import { combineReducers }    from 'redux';
import { routerStateReducer } from 'redux-router';
import customer            	  from './customer';
import branch  from './branches';

export default combineReducers({
  customer,
  branch,
  router: routerStateReducer
});
