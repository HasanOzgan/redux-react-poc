import React                 from 'react';
import { Route, IndexRoute } from 'react-router';
import CoreLayout            from '../layouts/CoreLayout';
import PageCustomer          from '../views/PageCustomer';
import PageNotFound          from '../views/PageNotFound';

export default (
  <Route path='/' component={CoreLayout}>
    <IndexRoute component={PageCustomer} />
    <Route path='*' component={PageNotFound} />
  </Route>
);
