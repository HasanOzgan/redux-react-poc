module.exports = {
  find: function () {
    return [{
      '_id': 2463,
      'title': 'Zümrütevler Şubesi',
      'town': 'Maltepe'
    }, {
      '_id': 2363,
      'title': 'Zeytinburnu Perdeciler Şubesi',
      'town': 'Zeytinburnu'
    }, {
      '_id': 1752,
      'title': 'Zeytinburnu İstasyon Şubesi',
      'town': 'Zeytinburnu'
    }, {
      '_id': 700,
      'title': 'Zeytinburnu Şubesi',
      'town': 'Zeytinburnu'
    }, {
      '_id': 2345,
      'title': 'Zeynep Kamil Şubesi',
      'town': 'Üsküdar'
    }, {
      '_id': 2493,
      'title': 'Zekeriyaköy Şubesi',
      'town': 'Sarıyer'
    }, {
      '_id': 2473,
      'title': 'Yüzyıl Şubesi',
      'town': 'Bağcılar'
    }, {
      '_id': 1682,
      'title': 'Yıldız Tabya Şubesi',
      'town': 'Gaziosmanpaşa'
    }, {
      '_id': 1648,
      'title': 'Yıldırım Mahallesi Şubesi',
      'town': 'Gaziosmanpaşa'
    }, {
      '_id': 2475,
      'title': 'Yıldırım Beyazıt Şubesi',
      'town': 'Bahçelievler'
    }, {
      '_id': 926,
      'title': 'Yeşilyurt Şubesi',
      'town': 'Bakırköy'
    }];
  },

  getTitleById: function (branchId) {
    const items = this.find();

    for (let ind = 0; ind < items.length; ind++) {
      const item = items[ind];
      if (item._id === branchId) {
        return item.title;
      }
    }
    return '';
  }
};
