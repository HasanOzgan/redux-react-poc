module.exports = {
  find: function () {
    return {
      1: {
        '_id': 1,
        'avatar': '/assets/images/avatars/0.jpg',
        'firstname': 'Rich',
        'lastname': ' Hickey',
        'branchId': 2463,
        'iban': 'TR46000100246311111110000'
      },
      2: {
        '_id': 2,
        'avatar': '/assets/images/avatars/1.jpg',
        'firstname': 'James',
        'lastname': 'Gosling',
        'branchId': 2363,
        'iban': 'TR460001002363011111110002'
      },
      3: {
        '_id': 3,
        'avatar': '/assets/images/avatars/2.jpg',
        'firstname': 'Dennis',
        'lastname': 'Ritchie',
        'branchId': 1752,
        'iban': 'TR460001001752011111110003'
      },
      4: {
        '_id': 4,
        'avatar': '/assets/images/avatars/3.jpg',
        'firstname': 'Martin',
        'lastname': 'Odersky',
        'branchId': 700,
        'iban': 'TR460001000700011111110000'
      },
      5: {
        '_id': 5,
        'avatar': '/assets/images/avatars/4.jpg',
        'firstname': 'Rob',
        'lastname': 'Pike',
        'branchId': 2345,
        'iban': 'TR460001002345011111110000'
      },
      6: {
        '_id': 6,
        'avatar': '/assets/images/avatars/6.jpg',
        'firstname': 'Anders ',
        'lastname': 'Hejlsberg',
        'branchId': 2493,
        'iban': 'TR460001002493011111110000'
      },
      7: {
        '_id': 7,
        'avatar': '/assets/images/avatars/5.jpg',
        'firstname': 'Grace',
        'lastname': 'Hopper',
        'branchId': 2473,
        'iban': 'TR460001002473011111110000'
      },
      8: {
        '_id': 8,
        'avatar': '/assets/images/avatars/9.jpg',
        'firstname': 'Ada',
        'lastname': 'Lovelace',
        'branchId': 1682,
        'iban': 'TR460001001682011111110000'
      },
      9: {
        '_id': 9,
        'avatar': '/assets/images/avatars/8.jpg',
        'firstname': 'Yukihiro',
        'lastname': 'Matsumoto',
        'branchId': 1648,
        'iban': 'TR460001001648011111110000'
      },
      10: {
        '_id': 10,
        'avatar': '/assets/images/avatars/7.jpg',
        'firstname': 'Bjarne',
        'lastname': 'Stroustrup',
        'branchId': 2475,
        'iban': 'TR460001002475011111110000'
      }
    };
  }
};
