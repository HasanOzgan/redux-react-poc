import React, {Component} from 'react';
import CustomerList from '../components/customer/list/CustomerList';
import CustomerPanel from '../components/customer/panel/CustomerPanel';

export default class PageCustomer extends Component {
  render () {
    return (
			<div>
				<div className='row'>
					<div className='customer-navbar col-lg-3 col-md-3 col-sm-4'>
						<CustomerList />
					</div>
					<div className='col-lg-9 col-md-9 col-sm-8'>
						<CustomerPanel />
					</div>
				</div>
			</div>
		);
  }
}
